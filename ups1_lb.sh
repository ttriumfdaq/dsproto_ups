#!/bin/sh

exec 1>>$HOME/online/ups1.log
exec 2>&1

echo --- ups1_lb started --------------------------- `date`

#date
#echo Stopping the run
#mtransition STOP &
#sleep 5

date
echo Shutting down EB
# odbedit -c "sh EB" &

date
echo ramping down HV...
# /home/dsproto/online/dsproto_...

date
echo Turning off VME crate...
# odbedit -c "set /equipment/dsvme01/settings/mainSwitch 0"
# snmpset -v 2c -M +/home/deap/pro/FrontEnd/slow/fewiener -m +WIENER-CRATE-MIB -c guru dsvme01 sysMainSwitch.0 i 0

date
echo Turning off CDU...
# snmpset -v 2c -M +/home/deap/pro/FrontEnd/slow/fesnmp -m +Sentry3-MIB -c write dscdu outletControlAction.1.1.2 i 2

# We need 1 minutes to be sure that all the channels have been ramped down.
# sleep 60

date
echo Turning off HV main switch...
# odbedit -c "set /equipment/deapmpod/settings/mainSwitch 0"
# snmpset -v 2c -M +/home/dsproto/online/bin/fewiener -m +WIENER-CRATE-MIB -c guru dshv sysMainSwitch.0 i 0

date
echo Shutting down MIDAS
# odbedit -c "sh mlogger" &
sleep 5
# killall mlogger mserver mhttpd
sleep 2

date
echo Shutting down ...
#sudo -n -b /sbin/poweroff

echo -------------------------- ups1_lb finished --- `date`

#end
