# Makefile
#
# $Id$
#

OSFLAGS  = -DOS_LINUX -Dextname
CFLAGS   = -g -O2 -fPIC -Wall -Wuninitialized -I. -I$(MIDASSYS)/include -DHAVE_LIBUSB -I$(MIDASSYS)/mscb -I$(MIDASSYS)/drivers/divers -I../shared
#CXXFLAGS = $(CFLAGS) -DHAVE_ROOT -DUSE_ROOT -I$(ROOTSYS)/include
CXXFLAGS = $(CFLAGS)

LIBS = -lm -lz -lutil -lnsl -lpthread -lrt

# MIDAS library
CFLAGS += -I$(MIDASSYS)/drivers/vme
MIDASLIBS = $(MIDASSYS)/lib/libmidas.a
MFE = $(MIDASSYS)/lib/mfe.o

# ROOT library
ROOTLIBS = $(shell $(ROOTSYS)/bin/root-config --libs) -lThread -Wl,-rpath,$(ROOTSYS)/lib
ROOTGLIBS = $(shell $(ROOTSYS)/bin/root-config --glibs) -lThread -Wl,-rpath,$(ROOTSYS)/lib

# ROOT analyzer library
ROOTANA = $(HOME)/packages/rootana
CFLAGS += -I$(ROOTANA) -DHAVE_MIDAS
ROOTGLIBS += -lXMLParser

#all:: dox
#all:: fefgdwater.exe
#all:: fefgdwater.d
#all:: fefgddccpwr.exe
#all:: fefgdsc.exe
#all:: fefgdsc.d
#all:: fefgdmscb.exe
#all:: fefgdmscb.d
#all:: fefgdwiener.exe
#all:: write_fgd_bias.exe
#all:: write_fgd_bias.d
#all:: write_fgd_asum.exe
#all:: write_fgd_asum.d
#all:: write_fgd_athr.exe
#all:: write_fgd_athr.d
all:: fenutups.exe
all:: fenutups.d
#all:: fexantrex.exe
#all:: feFGDCtmScm.exe
#all:: fefgd1watertemp.exe
#all:: feccbertan.exe

dox: ; doxygen ../shared/Doxyfile 

feFGDCtmScm.exe: feFGDCtmScm.o mscb.o mscbrpc.o musbstd.o $(MFE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb

fefgdwater.exe: %.exe: %.o ModbusTcp.o $(MIDASLIBS) $(MFE)
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIBS)

fefgddccpwr.exe: ../shared/fepwrswitch.cxx MscbDevice.o mscb.o mscbrpc.o musbstd.o $(MFE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb -DFE_NAME=\"fefgddccpwr\" -DEQ_NAME=\"FgdDccPwr\" -DEQ_EVID=EVID_FGDDCCPWR

fefgdwiener.exe: %.exe: ../shared/fewiener.cxx $(MIDASLIBS) $(MFE)
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIBS) -DFE_NAME=\"fefgdwiener\" -DEQ_NAME=\"FgdWiener\" -DEQ_EVID=EVID_FGDWIENER

fewiener.exe: %.exe: %.o $(MIDASLIBS) $(MFE)
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIBS)

fenutups.exe: %.exe: %.o $(MIDASLIBS) $(MFE)
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIBS)

fefgdcooling.exe: %.exe: %.o ModbusTcp.o $(MIDASLIBS) $(MFE)
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIBS)

fedvm.exe: %.exe: %.o $(MIDASLIBS) $(MFE)
	$(CXX) -o $@ $(CFLAGS) $(OSFLAGS) $^ $(MIDASLIBS) $(LIBS)

gscmonitor.exe: %.exe: %.o
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(ROOTANA)/librootana.a $(ROOTLIBS) $(MIDASLIBS) $(LIBS) -lusb

write_fgd_asum.exe write_fgd_bias.exe write_fgd_athr.exe: %.exe: %.o
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(ROOTANA)/librootana.a $(MIDASLIBS) $(LIBS) -lusb

mscb_scan.exe mscb_ping.exe: %.exe: %.o mscb.o mscbrpc.o musbstd.o
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb

mscbMakeCode.exe: %.exe: %.o mscb.o mscbrpc.o musbstd.o
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb

fefgdsc.exe: fefgdsc.o $(MFE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb

fefgdmscb.exe: fefgdmscb.o MscbDevice.o mscb.o mscbrpc.o musbstd.o $(MFE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb -lodbc

fepwrswitch.exe: fepwrswitch.o MscbDevice.o mscb.o mscbrpc.o musbstd.o $(MFE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb

fexantrex.exe: fexantrex.o mscb.o mscbrpc.o musbstd.o $(MFE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb

fefgd1watertemp.exe: fefgd1watertemp.o MscbDevice.o mscb.o mscbrpc.o musbstd.o $(MFE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb

feccbertan.exe: feccbertan.o MscbDevice.o mscb.o mscbrpc.o musbstd.o $(MFE)
	$(CXX) $(CFLAGS) $(OSFLAGS) -o $@ $^ $(MIDASLIBS) $(LIBS) -lusb

ModbusTcp.o: %.o: $(MIDASSYS)/drivers/divers/%.cxx
	$(CXX) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

mscbdev.o: %.o: $(MIDASSYS)/drivers/device/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -DHAVE_LIBUSB -I$(MIDASSYS)/mscb -c -o $@ -c $<

musbstd.o: %.o: $(MIDASSYS)/drivers/usb/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -DHAVE_LIBUSB -I$(MIDASSYS)/mscb -c -o $@ -c $<

mscbbus.o: %.o: $(MIDASSYS)/drivers/bus/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -DHAVE_LIBUSB -I$(MIDASSYS)/mscb -c -o $@ -c $<

mscbrpc.o mscb.o: %.o: $(MIDASSYS)/mscb/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -DHAVE_LIBUSB -I$(MIDASSYS)/mscb -I$(MIDASSYS)/../mxml -c -o $@ -c $<

mxml.o: $(MIDASSYS)/../mxml/mxml.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

strlcpy.o: $(MIDASSYS)/../mxml/strlcpy.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c -o $@ -c $<

%.o: %.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $<

%.o: $(MIDASSYS)/drivers/vme/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $<

%.o: $(MIDASSYS)/drivers/vme/vmic/%.c
	$(CC) $(CFLAGS) $(OSFLAGS) -c $<

client_main.o: %.o: %.c
	$(CXX) $(CXXFLAGS) $(OSFLAGS) -c $<

%.o: %.cxx
	$(CXX) $(CXXFLAGS) $(OSFLAGS) -I$(ROOTSYS)/include -c $<

%.o: ../shared/%.cxx
	$(CXX) $(CXXFLAGS) $(OSFLAGS) -I$(ROOTSYS)/include -c $<

%.d: %.cxx
	$(CXX) -MM -MD $(CXXFLAGS) $(OSFLAGS) -I$(ROOTSYS)/include -c $<

%.d: %.c
	$(CC) -MM -MD $(CFLAGS) $(OSFLAGS) -c $<

$(MFE) $(MIDASLIBS):
	@cd $(MIDASSYS) && make

depend:

-include *.d

clean::
	rm -f *.o *.exe
	rm -f *.d
	rm -f *~

# end
