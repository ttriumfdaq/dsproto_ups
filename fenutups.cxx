/********************************************************************\

  Name:         fenutups.cxx
  Created by:   K.Olchanski

  Contents:     Frontend for UPS attached through the NUT package

  $Id$

\********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <string>
#include <sys/time.h>
#include <assert.h>
#include <math.h>
#include <time.h>

#include <vector>

#include "midas.h"
#include "msystem.h"
#include "mfe.h"


//#include "evid.h"

#ifndef FE_NAME
#define FE_NAME "fenutups"
#endif

#ifndef EQ_NAME
#define EQ_NAME "NutUps"
#endif

#ifndef EQ_EVID
#define EQ_EVID 10
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
   const char *frontend_name = FE_NAME;
/* The frontend file name, don't change it */
   const char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = TRUE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 0;

/* maximum event size produced by this frontend */
   INT max_event_size = 200*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 1024*1024;

/* buffer size to hold events */
   INT event_buffer_size = 1*1024*1024;

//  extern HNDLE hDB;

/*-- Function declarations -----------------------------------------*/
  INT frontend_pre_init(int argc, char* argv[]);
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_nutups_event(char *pevent, INT off);

/*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {

    {EQ_NAME "%02d",          /* equipment name */
     {EQ_EVID, (1<<EQ_EVID),  /* event ID, trigger mask */
      "SYSTEM",               /* event buffer */
      EQ_PERIODIC,            /* equipment type */
      0,                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS,              /* when to read this event */
      10000,                  /* poll time in milliseconds */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      1,                      /* whether to log history */
      "", "", "",}
     ,
     read_nutups_event,       /* readout routine */
     NULL,
     NULL,
     NULL,       /* bank list */
    }
    ,
    {""}
  };
  
/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

#include "utils.cxx"

/*-- Global variables ----------------------------------------------*/

static std::string gMscbDev;

/*-- Frontend Init -------------------------------------------------*/

std::string gNutUpsDev;
std::string gNutUpsSerial;
std::string gAlarmName;

static char eq_name[NAME_LENGTH];
INT frontend_index;

INT frontend_init()
{
  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  cm_deregister_transition(TR_START);
  cm_deregister_transition(TR_STOP);
  cm_deregister_transition(TR_PAUSE);
  cm_deregister_transition(TR_RESUME);

  cm_set_watchdog_params(TRUE, 120000);

  frontend_index = get_frontend_index();

  if (frontend_index < 1)
    {
      cm_msg(MERROR, frontend_name, "frontend_init(): Frontend index %d is not valid, please start with \'-i 1\'", frontend_index);
      return !SUCCESS;
    }

  sprintf(eq_name, "%s%02d", EQ_NAME, frontend_index);
  assert(strlen(eq_name) < NAME_LENGTH);

  char str[1024];
  sprintf(str, "/Equipment/" EQ_NAME "%02d/Settings/Nut UPS device", frontend_index);

  gNutUpsDev = odbReadString(str, 0, "", 200);

  if (gNutUpsDev.length() < 2)
    {
      cm_msg(MERROR, frontend_name, "frontend_init(): Nut UPS device name is blank, please set \'%s\'", str);
      return !SUCCESS;
    }

  sprintf(str, "/Equipment/" EQ_NAME "%02d/Settings/Nut UPS serial number", frontend_index);

  gNutUpsSerial = odbReadString(str, 0, "", 200);

  sprintf(str, "/Equipment/" EQ_NAME "%02d/Settings/Alarm name", frontend_index);

  gAlarmName = odbReadString(str, 0, "", 200);

  if (gNutUpsDev.length() < 2)
    {
      cm_msg(MERROR, frontend_name, "frontend_init(): Nut UPS device name is blank, please set \'%s\'", str);
      return !SUCCESS;
    }

  if (gAlarmName.length() < 1)
    {
      sprintf(str, EQ_NAME "%02d_%s", frontend_index, gNutUpsDev.c_str());
      gAlarmName = str;
    }

  int status = al_reset_alarm(gAlarmName.c_str());
  if (status != AL_SUCCESS)
    {
      al_trigger_alarm(gAlarmName.c_str(), "create alarm", "Warning", "", AT_INTERNAL);
      al_reset_alarm(gAlarmName.c_str());
    }

  set_equipment_status(eq_name, "Init Ok", "#00FF00");

  return SUCCESS;
}

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  ss_sleep(10);
  return SUCCESS;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Interrupt configuration ---------------------------------------*/
INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
  assert(!"interrupt_configure() is not implemented");
  return 0;
}

void Replace(std::string&x, const char* replace, const char* with)
{
  int i=x.find(replace);
  if (i>=0)
    x.replace(i, strlen(replace), with);
}

bool isNumeric(const char* s)
{
int countDot = 0;
  for (; *s != 0; s++)
    {
      if (isdigit(*s))
	continue;
      if (*s == ' ')
	continue;
      if (*s == '.'){
    countDot++;
	continue;
	}
      if (*s == '-')
	continue;
      if (*s == '\n')
	return true;
      return false;
    }
	if (countDot> 1) return false;
  return true;
}

bool gEnableControl = false;
int  gDebug = 0;

extern HNDLE hDB;
static HNDLE hVar = 0;
static HNDLE hSet = 0;
static HNDLE hRdb = 0;
static HNDLE hStatus = 0;

#define MAX_VARS 100

static int odb_nvars = 0;

static char names[MAX_VARS*NAME_LENGTH];
static double values[MAX_VARS];

static time_t gOBtime  = 0;
static int    gOBdelay = 0;
static int    gOBflag  = 0;
static int    gLBflag  = 0;
static int    gLBthrPct = 0;
static std::string gOBscript;
static std::string gLBscript;

static void update_settings(INT a, INT b, void *c)
{
  char str[1024];

  printf("update_settings!\n");

  sprintf(str, "/Equipment/" EQ_NAME "%02d/Settings/EnableControl", frontend_index);

  gEnableControl = odbReadInt(str, 0, 0);

  sprintf(str, "/Equipment/" EQ_NAME "%02d/Settings/Debug", frontend_index);

  gDebug = odbReadInt(str, 0, 0);

  sprintf(str, "/Equipment/%s/Settings/UpsOnBatteryDelay", eq_name);

  gOBdelay = odbReadInt(str, 0, 60);
  if (gOBdelay < 0)
    gOBdelay = 0;

  sprintf(str, "/Equipment/%s/Settings/UpsOnBatteryScript", eq_name);

  gOBscript = odbReadString(str, 0, "", 250);

  sprintf(str, "/Equipment/%s/Settings/UpsLowBatteryThresholdPct", eq_name);

  gLBthrPct = odbReadInt(str, 0, 10);

  if (gLBthrPct > 90)
    gLBthrPct = 90;

  if (gLBthrPct < 10)
    gLBthrPct = 10;

  sprintf(str, "/Equipment/%s/Settings/UpsLowBatteryScript", eq_name);

  gLBscript = odbReadString(str, 0, "", 250);
}

static void open_hotlink(HNDLE hDB, HNDLE hSet)
{
  static bool once = false;
  if (once)
    return;
  once = true;

  update_settings(0,0,0);

  int status = db_open_record(hDB, hSet, NULL, 0, MODE_READ, update_settings, NULL);
  assert(status == DB_SUCCESS);
}

int read_nutups_event(char *pevent, INT off)
{
  time_t now;
  time(&now);

  if (gOBflag==1) {
    if (now >= gOBtime) {
      cm_msg(MERROR, frontend_name, "Running the UpsOnBattery script: %s", gOBscript.c_str());
      ss_system(gOBscript.c_str());
      gOBflag = 2;
    }
  }

  if (hVar==0)
    {
      char str[1024];
      sprintf(str, "/Equipment/" EQ_NAME "%02d/Variables", frontend_index);

      int status = db_find_key(hDB, 0, str, &hVar);
      if (status == DB_NO_KEY)
	{
	  status = db_create_key(hDB, 0, str, TID_KEY);
	  if (status == DB_SUCCESS)
	    status = db_find_key(hDB, 0, str, &hVar);
	}

      if (status != SUCCESS)
	{
	  cm_msg(MERROR, frontend_name, "read_nutups_event: Cannot find or create %s, status %d, exiting", str, status);
	  exit(1);
	}
    }

  if (hSet==0)
    {
      char str[1024];
      sprintf(str, "/Equipment/" EQ_NAME "%02d/Settings", frontend_index);

      int status = db_find_key(hDB, 0, str, &hSet);
      if (status == DB_NO_KEY)
	{
	  status = db_create_key(hDB, 0, str, TID_KEY);
	  if (status == DB_SUCCESS)
	    status = db_find_key(hDB, 0, str, &hSet);
	}

      if (status != SUCCESS)
	{
	  cm_msg(MERROR, frontend_name, "read_nutups_event: Cannot find or create %s, status %d, exiting", str, status);
	  exit(1);
	}
    }

  if (hRdb==0)
    {
      char str[1024];
      sprintf(str, "/Equipment/" EQ_NAME "%02d/Readback", frontend_index);

      int status = db_find_key(hDB, 0, str, &hRdb);
      if (status == DB_NO_KEY)
	{
	  status = db_create_key(hDB, 0, str, TID_KEY);
	  if (status == DB_SUCCESS)
	    status = db_find_key(hDB, 0, str, &hRdb);
	}

      if (status != SUCCESS)
	{
	  cm_msg(MERROR, frontend_name, "read_nutups_event: Cannot find or create %s, status %d, exiting", str, status);
	  exit(1);
	}
    }

  if (hStatus==0)
    {
      char str[1024];
      sprintf(str, "/Equipment/" EQ_NAME "%02d/Status", frontend_index);

      int status = db_find_key(hDB, 0, str, &hStatus);
      if (status == DB_NO_KEY)
	{
	  status = db_create_key(hDB, 0, str, TID_KEY);
	  if (status == DB_SUCCESS)
	    status = db_find_key(hDB, 0, str, &hStatus);
	}

      if (status != SUCCESS)
	{
	  cm_msg(MERROR, frontend_name, "read_nutups_event: Cannot find or create %s, status %d, exiting", str, status);
	  exit(1);
	}
    }

  //
  // Control commands:
  //
  // /opt/nut-2.4.1/bin/upsc upsusb0@localhost
  //

  char str[1024];
  //sprintf(str, "/opt/nut-2.4.1/bin/upsc %s 2>&1", gNutUpsDev.c_str());
  //  sprintf(str, "/opt/nut/bin/upsc %s 2>&1", gNutUpsDev.c_str());
  sprintf(str, "/usr/local/ups/bin/upsc %s 2>&1", gNutUpsDev.c_str());

  if (gDebug)
    printf("Read UPS event: %s\n", str);

  int have_alarm = 0;

  int ivar = 0;

  char ups_alarm[256];
  char ups_status[256];
  double ups_charge_pct = 0;
  double ups_runtime_min100 = 0;

  static bool ups_data_is_stale = false;

  FILE *fp = popen(str, "r");
  if (fp == NULL)
    {
      ss_sleep(200);
      return 0;
    }

  while (1)
    {
      char *s = fgets(str, sizeof(str), fp);
      if (!s)
	break;

      //printf("upsc: %s\n", s);

      s = strchr(str, '\n');
      if (s)
	*s = 0;

      if (strstr(str, "Error"))
	{
	  if (strstr(str, "Data stale"))
	    {
	      if (!ups_data_is_stale) {
		cm_msg(MERROR, frontend_name, "UPS data is stale, upsc error: %s", str);

		char xstr[1024];
		sprintf(xstr, "%s UPS is off? upsc error: %s", gAlarmName.c_str(), str);
		al_trigger_alarm(gAlarmName.c_str(), xstr, "Warning", "", AT_INTERNAL);
		ss_sleep(1000);
	      }

	      pclose(fp);
	      ups_data_is_stale = true;

	      set_equipment_status(eq_name, "Data is stale", "#FF0000");
	      return 0;
	    }

	  cm_msg(MERROR, frontend_name, "read_nutups_event: upsc error: %s", str);
          char xstr[1024];
          sprintf(xstr, "%s upsc error: %s", gAlarmName.c_str(), str);
          al_trigger_alarm(gAlarmName.c_str(), xstr, "Warning", "", AT_INTERNAL);
	  ss_sleep(1000);
	  pclose(fp);

	  set_equipment_status(eq_name, "Communication error", "#FF0000");
	  return 0;
	}

      if (strstr(str, "upscli not initialized"))
	{
	  continue;
	}

      if (strstr(str, "Can not connect to localhost in SSL"))
	{
	  continue;
	}


      s = strchr(str, '.');
      if (s == NULL)
	{
	  printf("unknown response (no dot): %s", str);
	  continue;
	}

      s = strchr(str, ':');
      if (s == NULL)
	{
	  printf("unknown response (no colon): %s", str);
	  continue;
	}

      *s = 0;
      s++;

      while (isspace(*s))
	s++;
		  
      char* name = str;
      char* value = s;
      bool  numeric = isNumeric(value);
      double dvalue = atof(value);

      if (gDebug)
	printf("name [%s] value [%s], numeric %d, value %f\n", name, value, numeric, dvalue);

      if (strcmp(name, "ups.serial") == 0)
	{
	  numeric = false;

	  if (gNutUpsSerial.length() == 0)
	    {
	      db_set_value(hDB, hSet, "Nut UPS serial number", value, strlen(value)+1, 1, TID_STRING);

	      gNutUpsSerial = value;												   
	    }

	  if (gNutUpsSerial != value)
	    {
	      cm_msg(MERROR, frontend_name, "read_nutups_event: UPS Serial number mismatch: ODB has \'%s\', UPS reports \'%s\', perhaps we are talking to the wrong serial port or the wrong computer", gNutUpsSerial.c_str(), value);
	      cm_disconnect_experiment();
	      exit(1);
	    }
	}

      if (strlen(name) >= NAME_LENGTH-1)
	{
	  cm_msg(MERROR, frontend_name, "read_nutups_event: Variable name \'%s\' is too long %d, limit %d", name, (int)strlen(name), NAME_LENGTH-1);
	  exit(1);
	}

      if (numeric)
	db_set_value(hDB, hRdb, name, &dvalue, sizeof(dvalue), 1, TID_DOUBLE);
      else
	db_set_value(hDB, hRdb, name, value, strlen(value)+1, 1, TID_STRING);

      if (strstr(name, "status"))
	{
	  dvalue = -1;

	  if (strcmp(value, "On")==0)
	    dvalue = 1;
	  else if (strcmp(value, "OL")==0)
	    dvalue = 2;
	  else if (strcmp(value, "ALARM OB")==0)
	    dvalue = 3;
	  else if (strcmp(value, "ALARM OB LB")==0)
	    dvalue = 4;

	  numeric = true;
	}

      if (numeric)
	{
	  memcpy(&names[ivar*NAME_LENGTH], name, NAME_LENGTH);
	  values[ivar] = dvalue;
	  ivar++;
	}
      else
	{
	}

      if (strcmp(name, "ups.alarm")==0)
	{
	  have_alarm = 1;

          char str[1024];
          sprintf(str, "%s alarm: %s", gAlarmName.c_str(), value);
          al_trigger_alarm(gAlarmName.c_str(), str, "Warning", "", AT_INTERNAL);

          strlcpy(ups_alarm, value, sizeof(ups_alarm));
	}

      if (strcmp(name, "ups.status")==0)
	{
	  int interlock_ok = 0;

	  if (strcmp(value, "OL") != 0)
	    interlock_ok = 1;

	  db_set_value(hDB, hStatus, "interlock_ok", &interlock_ok, sizeof(int), 1, TID_INT);

          strlcpy(ups_status, value, sizeof(ups_status));
	}

      if (strcmp(name, "battery.charge")==0)
	{
	  ups_charge_pct = dvalue;
	}

      if (strcmp(name, "battery.runtime")==0)
	{
	  ups_runtime_min100 = dvalue;
	}
    }

  pclose(fp);

  if (ups_data_is_stale) {
    ups_data_is_stale = false;
    cm_msg(MINFO, frontend_name, "UPS data is okey now");
  }

  if (!have_alarm)
    {
      db_set_value(hDB, hRdb, "ups.alarm", "", 1, 1, TID_STRING);

      al_reset_alarm(gAlarmName.c_str());

      ups_alarm[0] = 0;
    }

  time(&now);

  static char saved_ups_alarm[256];
  static char saved_ups_status[256];

  //printf("ups status [%s]\n", ups_status);
  //printf("ups alarm [%s]\n", ups_alarm);

  if (strcmp(ups_alarm, saved_ups_alarm) != 0)
     {
        cm_msg(MINFO, frontend_name, "UPS alarm changed from [%s] to [%s], charge %.0f%%, run time %.1fmin", saved_ups_alarm, ups_alarm, ups_charge_pct, ups_runtime_min100/100.0);
        strlcpy(saved_ups_alarm, ups_alarm, sizeof(ups_alarm));
     }

  bool maybe_low_charge = (ups_charge_pct < gLBthrPct);
  bool low_charge = false;

  static int gCountLowCharge = 0;
  static time_t gTimeLowCharge = 0;

  if (!maybe_low_charge) {
    gCountLowCharge = 0;
    gTimeLowCharge = now + 45;
  } else {
    gCountLowCharge++;
    cm_msg(MERROR, frontend_name, "UPS low charge condition detected, status [%s], charge %.0f%%, run time %.1fmin, ODB low charge threshold %d%%, count %d, %d sec before shutdown", ups_status, ups_charge_pct, ups_runtime_min100/100.0, gLBthrPct, gCountLowCharge, (int)(gTimeLowCharge - now));

    if (now >= gTimeLowCharge)
      low_charge = true;
  }

  bool maybe_status_LB = (strstr(ups_status, "LB") != NULL);
  bool status_LB = false;

  static int gCountStatusLB = 0;
  static time_t gTimeStatusLB = 0;

  //if (gTimeStatusLB != 0)
  //  maybe_status_LB = true;

  if (!maybe_status_LB) {
    gCountStatusLB = 0;
    gTimeStatusLB = now + 45;
  } else {
    gCountStatusLB++;
    cm_msg(MERROR, frontend_name, "UPS low battery status (LB) condition detected, status [%s], charge %.0f%%, run time %.1fmin, count %d, %d sec before shutdown", ups_status, ups_charge_pct, ups_runtime_min100/100.0, gCountStatusLB, (int)(gTimeStatusLB - now));

    if (now >= gTimeStatusLB)
      status_LB = true;
  }

  bool status_OB = (strstr(ups_status, "OB") != NULL);

  //status_OB = true;

  if (!maybe_status_LB) {
  } else {
    cm_msg(MERROR, frontend_name, "UPS on-battery status (OB) condition detected, status [%s], charge %.0f%%, run time %.1fmin", ups_status, ups_charge_pct, ups_runtime_min100/100.0);
  }

  if (1)
    printf(">> ups status : %s \'%s\', charge %d thr %d, low_charge %d, status_LB %d, status_OB %d - len: saved:%d - %d\n", saved_ups_status, ups_status, (int)ups_charge_pct, gLBthrPct, low_charge, status_LB, status_OB
	   , strlen(saved_ups_status), strlen(ups_status));

    if (strcmp(ups_status, saved_ups_status) != 0) {
      printf("ups status %s -  \'%s\', charge %d thr %d, low_charge %d, status_LB %d, status_OB %d\n", saved_ups_status, ups_status, (int)ups_charge_pct, gLBthrPct, low_charge, status_LB, status_OB);
      cm_msg(MINFO, frontend_name, "UPS status changed from [%s] to [%s], charge %.0f%%, run time %.1fmin", saved_ups_status, ups_status, ups_charge_pct, ups_runtime_min100/100.0);
      strlcpy(saved_ups_status, ups_status, sizeof(ups_status));
    }

  if (status_OB) {
    // UPS is "on battery" - AC power lost
    
    if (!gOBflag) {
      gOBflag = 1;
      gOBtime = now + gOBdelay;
      cm_msg(MERROR, frontend_name, "UPS is in on-battery mode (OB), the UpsOnBattery script will run in %d seconds", gOBdelay);
    }

    if (status_LB || low_charge) {
      // UPS "battery low"

      if (!gLBflag) {
	gLBflag = 1;
	cm_msg(MERROR, frontend_name, "UPS is out of juice, status \'%s\', charge %.0f%%, running the UpsLowBattery script: %s", ups_status, ups_charge_pct, gLBscript.c_str());
	ss_system(gLBscript.c_str());
      }
    }
  } else {
    // AC power is restored, cancel the OB script, clear the LB flag
    gLBflag = 0;
    if (gOBflag) {
      gOBflag = 0;
      cm_msg(MINFO, frontend_name, "UPS external power is back, UpsOnBattery script canceled");
    }
  }

  if (1)
    {
      char s[256];
      sprintf(s, "Status: %s, %.0f%%, %.1fmin", ups_status, ups_charge_pct, ups_runtime_min100/100.0);
      set_equipment_status(eq_name, s, "#00FF00");
    }

  int nvars = ivar;

  db_set_value(hDB, hVar, "Values", values, nvars*sizeof(double), nvars, TID_DOUBLE);

  if (odb_nvars == 0)
    {
      HNDLE hKey;
      int status = db_find_key(hDB, hSet, "Names Values", &hKey);

      if (status == DB_SUCCESS)
	{
	  KEY key;
	  status = db_get_key(hDB, hKey, &key);
	  assert(status == DB_SUCCESS);

	  odb_nvars = key.num_values;
	}
    }

  if (gDebug)
    printf("found %d variables, in odb have %d variables\n", nvars, odb_nvars);

  if (nvars != odb_nvars)
    {
      odb_nvars = nvars;
      db_set_value(hDB, hSet, "Names Values", names, nvars*NAME_LENGTH, nvars, TID_STRING);
    }

  open_hotlink(hDB, hSet);
  
  return 0;
}

INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  assert(!"poll_event is not implemented");
  return 0;
}

//end
